-- TODO: Add VHDL Header here (in Emacs use: VHDL->Template->Insert Header )
--       Use your group number and name(s) of the group member(s)
--       in the 'author' field
--       Testbench has an example what a good header should look like

----**************Hassan, Hamid***********	
--**************LOgic Synthesis**************
--**************Exercise two ********************
--**************Ripple Carry adder**************
-- TODO: Add library called ieee here
--       And use package called std_logic_1164 from the library

library IEEE;
use IEEE.std_logic_1164.all;


entity ripple_carry_adder is 
      
-- Name: ripple_carry_adder
-- No generics yet
port (a_in, b_in: in STD_LOGIC_VECTOR(2 downto 0);
	s_out: out STD_LOGIC_VECTOR(3 downto 0);
);

-------------------------------------------------------------------------------

-- Architecture called 'gate' is already defined. Just fill it.
-- Architecture defines an implementation for an entity
--architecture gate of ripple_carry_adder is
--component full_adder_vhdl code


  -- TODO: Add your internal signal declarations here
signal c,d,e,f,g,h, Carry_ha,Carry_fa:std_logic;
-----  signal assignmet should be acording to the diagram or just a common 4bit ripplr cary adder,
begin  -- gate


s_out(0) <= a_in(0) xor b_in(0);
Carry_ha <= a_in(0) and b_in(0);

C  <= a_in(1) xor b_in(1);
--s_out(1) <= Carry_ha xor C;
Carry_ha xor C <= s_out(1);
Carry_ha and C <= D;
E <= a_in(1) and b_in(1);
Carry_fa <= D or E;

F <= a_in(2) xor b_in(2);
Carry_fa xor F <= s_out(2);
G <= Carry_fa and F;
H <= a_in(2) and b_in(2);
s_out(3) = G or H;

  -- TODO: Add signal assignments here
  -- x(0) <= y and z(2);
  -- Remember that VHDL signal assignments happen in parallel
  -- Don't use processes
    
end gate;
